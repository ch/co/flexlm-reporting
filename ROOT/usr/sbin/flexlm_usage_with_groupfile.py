#!/usr/bin/python3
import sys

from flexlmusage import SchrodStats

try:
    csvfile = sys.argv[1]
except IndexError:
    print("Usage: " + sys.argv[0] + " groupfile.csv")
    sys.exit(1)
stats = SchrodStats(csvfile)
stats.collect_from_logs(pattern="*")
stats.populate_groups()
print(stats.html_report())
