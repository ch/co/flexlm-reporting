#!/usr/bin/python3
from flexlmusage import FlexLMStats

stats = FlexLMStats()
stats.collect_from_logs(pattern="flexlm-combined*")
stats.populate_groups()
print(stats.html_report("Intel"))
