"""
A collection of things to parse our FlexLM logs.[1]

from flexlm_usage import FlexLMStats
stats = FlexLMStats()
stats.collect_from_logs(pattern='*vendordaemon*') # or whatever
stats.populate_groups()

and then play about with the data that's inside s .

[1] FlexLM say we shouldn't use FlexLM logs for getting licence usage.
But it's all we've got.
"""

import csv
import datetime
import glob
import gzip
import os
import pickle
import re
from collections import OrderedDict, defaultdict, namedtuple

import jinja2
import psycopg2

html_boilerplate = """
<html>
 <body>
  {{ body }}
 </body>
</html>
"""


machines = {}
groups = {}

pretty_daemons = {
    "INTEL": "Intel",
    "pgroupd": "PGI",
    "MLM": "Matlab",
    "orglab": "Origin",
}

pretty_licences = {
    "CCompL": "C compiler",
    "FCompL": "Fortran compiler",
}

dbuser = "licence_reports"


class CheckOut(namedtuple("CheckOut", ["licence", "user", "group", "daemon", "count"])):
    pass


class FlexLMStats:
    def __init__(self):
        self.checkouts_by_person = defaultdict(dict)
        self.checkouts_by_group = defaultdict(dict)
        self.checkouts_per_vendor_by_group = defaultdict(dict)
        self.checkouts_with_unknown_group = defaultdict(dict)
        self.users_and_group = {}
        self.all_checkouts = defaultdict(dict)
        self.all_detailed_checkouts = list()
        self.all_denied = defaultdict(dict)
        self.denied_by_person = defaultdict(dict)

    def run(self, logdir="/var/log/flexlm", pattern="*"):
        self.collect_from_logs(logdir, pattern)
        self.populate_groups()
        print(self.html_report())

    def save_stats(self, filename):
        with open(filename, "wb") as f:
            pickle.dump(self, f)
        # from flexlm_usage import FlexLMStats
        # import pickle
        # f=open(whatever,'rb')
        # s=pickle.load(f)

    def collect_from_logs(self, logdir="/var/log/flexlm", pattern="*"):
        """
        Assumes the only logs we are intersted in are in files under the logdir.
        Does not search down the tree.
        """
        logdir = os.path.join(logdir, pattern)
        logfile = glob.glob(logdir)
        # by default we only look at last 12 months worth
        now = datetime.datetime.today()
        cutoff_date = now - datetime.timedelta(days=365)
        for log in logfile:
            if os.path.isdir(log):
                continue
            # log = os.path.join(logdir,logname)
            sdata = os.stat(log)
            if datetime.datetime.fromtimestamp(sdata.st_mtime) > cutoff_date:
                if log.endswith("gz"):
                    f = gzip.open(log, "rt")
                else:
                    f = open(log, "r")
                for line in f:
                    # line = eline.decode('utf-8')
                    if " OUT:" in line:
                        daemon = re.findall(r"\((\S+)\).* OUT:", line)[0]
                        try:
                            daemon = pretty_daemons[daemon]
                        except KeyError:
                            pass
                        toks = line.split()
                        licence = toks[3]
                        try:
                            licence = pretty_licences[licence]
                        except KeyError:
                            pass
                        requestor = " ".join(toks[4:])
                        (user, trailingjunk, otherjunk) = requestor.partition("@")
                        try:
                            number_licences = re.findall(
                                r".*\((\d+) licenses\)", trailingjunk
                            )[0]
                        except IndexError:
                            number_licences = 1
                        self.checkouts_by_person[daemon].setdefault(licence, {})
                        self.checkouts_by_group[daemon].setdefault(licence, {})
                        self.checkouts_by_person[daemon][licence].setdefault(user, 0)
                        self.checkouts_by_person[daemon][licence].setdefault(
                            "anyone", 0
                        )
                        self.checkouts_by_person[daemon][licence][user] += int(
                            number_licences
                        )
                        self.checkouts_by_person[daemon][licence]["anyone"] += int(
                            number_licences
                        )
                        self.all_checkouts[daemon].setdefault(user, 0)
                        self.all_checkouts[daemon].setdefault("anyone", 0)
                        self.all_checkouts[daemon][user] += int(number_licences)
                        self.all_checkouts[daemon]["anyone"] += int(number_licences)
                    if "DENIED:" in line:
                        try:
                            daemon = re.findall(r"\((\w+)\) DENIED:", line)[0]
                        except IndexError:
                            continue
                        try:
                            daemon = pretty_daemons[daemon]
                        except KeyError:
                            pass
                        toks = line.split()
                        licence = toks[3]
                        try:
                            licence = pretty_licences[licence]
                        except KeyError:
                            pass
                        requestor = " ".join(toks[4:])
                        possible_usernames = re.split(r" ([\w]+)@[\S]+ ", requestor)
                        if possible_usernames and len(possible_usernames) > 2:
                            user = possible_usernames[1]
                        else:
                            continue
                        try:
                            number_licences = re.findall(
                                r".*\((\d+) licenses\)", trailingjunk
                            )[0]
                        except IndexError:
                            number_licences = 1
                        self.denied_by_person[daemon].setdefault(licence, {})
                        self.denied_by_person[daemon][licence].setdefault(user, 0)
                        self.denied_by_person[daemon][licence].setdefault("anyone", 0)
                        self.denied_by_person[daemon][licence][user] += int(
                            number_licences
                        )
                        self.denied_by_person[daemon][licence]["anyone"] += int(
                            number_licences
                        )
                        self.all_denied[daemon].setdefault(user, 0)
                        self.all_denied[daemon].setdefault("anyone", 0)
                        self.all_denied[daemon][user] += int(number_licences)
                        self.all_denied[daemon]["anyone"] += int(number_licences)
                f.close()

    def populate_groups(self):
        db = psycopg2.connect(
            "host=database.ch.private.cam.ac.uk dbname=chemistry user={0}".format(
                dbuser
            )
        )
        c = db.cursor()
        c.execute("set datestyle to 'European,SQL'")
        for daemon in self.checkouts_by_person:
            for licence in self.checkouts_by_person[daemon]:
                for user in self.checkouts_by_person[daemon][licence]:
                    sql = (
                        "select name from apps.software_licence_stats where crsid = %s"
                    )
                    c.execute(sql, [user])
                    try:
                        group = c.fetchone()[0]
                    except TypeError:
                        group = "Unknown"
                        self.checkouts_with_unknown_group[daemon].setdefault(
                            licence, {}
                        )
                        self.checkouts_with_unknown_group[daemon][licence].setdefault(
                            user, 0
                        )
                        self.checkouts_with_unknown_group[daemon][licence][
                            user
                        ] += self.checkouts_by_person[daemon][licence][user]
                    self.users_and_group[user] = group
                    self.checkouts_by_group[daemon][licence].setdefault(group, 0)
                    self.checkouts_by_group[daemon][licence][
                        group
                    ] += self.checkouts_by_person[daemon][licence][user]
                    self.checkouts_per_vendor_by_group[daemon].setdefault(group, 0)
                    self.checkouts_per_vendor_by_group[daemon][
                        group
                    ] += self.checkouts_by_person[daemon][licence][user]
                    self.all_detailed_checkouts.append(
                        CheckOut(
                            user=user,
                            group=group,
                            daemon=daemon,
                            licence=licence,
                            count=self.checkouts_by_person[daemon][licence][user],
                        )
                    )
        db.close()

    def html_per_user_report_for_group(self, groupname):
        """
        Generate a per user report for a group

        Came about because Katherine Abell wanted to break down her
        group's SCHROD usage.
        Use:
        s = FlexLMStats() / SchrodStats(csv)
        s.collect_from_logs()
        s.populate_groups()
        print(s.html_per_user_report_for_group('Abell'))

        """

        report_template = """
<html>
 <body>
  <h1>Checkouts by user for {{ groupname }} group</h1>
{% for user,checkouts in summary.items() -%}
    <h2>{{ user }}</h2>
    <table border="1">
     <tr>
      <th>Licence</th>
      <th>Count</th>
     </tr>

{%- for licence,count in checkouts.items() %}
     <tr>
      <td>{{ licence }}</td>
      <td>{{ count }}</td>
     </tr>
{%- endfor %}
    </table>

{%- endfor %}
 </body>
</html>
        """
        group_checkouts = [
            c for c in self.all_detailed_checkouts if c.group == groupname
        ]
        summary = defaultdict(dict)
        for c in group_checkouts:
            summary[c.user].setdefault(c.licence, 0)
            summary[c.user][c.licence] += c.count
        template = jinja2.Template(report_template)
        return template.render(groupname=groupname, summary=summary)

    def html_checkouts_by_group(self, daemon=None):
        report_template = """
  <h1>Checkouts by group</h1>
{%- for daemon,licences in data.items() %}
   <h2>{{ daemon }}</h2>
{%- for licence,checkouts in licences.items() %}
    <h3>{{ licence }}</h3>
    <table border="1">
      <tr>
       <th>Group</th>
       <th>Checkouts</th>
      </tr>
{%- for group, count in checkouts.items() %}
      <tr>
       <td>{{ group }}</td>
       <td>{{ count }}</td>
      </tr>

{% endfor %}
    </table>
{%- endfor %}
{%- endfor %}
        """
        template = jinja2.Template(report_template)
        sorted_checkouts = defaultdict(dict)
        for daemon_key, licence_dict in self.checkouts_by_group.items():
            for licence, group_checkouts in licence_dict.items():
                sorted_checkouts[daemon_key][licence] = OrderedDict(
                    sorted(
                        group_checkouts.items(), key=lambda t: int(t[1]), reverse=True
                    )
                )

        if daemon:
            filtered_checkouts_by_group = {
                k: v for k, v in sorted_checkouts.items() if k == daemon
            }
            return template.render(data=filtered_checkouts_by_group)
        return template.render(data=sorted_checkouts)

    def html_checkouts_per_vendor_by_group(self, daemon=None):
        report_template = """
  <h1>Checkouts per vendor by group</h1>
{%- for daemon,checkouts in data.items() %}
   <h2>{{ daemon }}</h2>
    <table border="1">
      <tr>
       <th>Group</th>
       <th>Checkouts</th>
      </tr>
{%- for group, count in checkouts.items() %}
      <tr>
       <td>{{ group }}</td>
       <td>{{ count }}</td>
      </tr>

{% endfor %}
     </table>
{%- endfor %}
"""
        template = jinja2.Template(report_template)
        sorted_checkouts = {
            k: OrderedDict(sorted(v.items(), key=lambda t: int(t[1]), reverse=True))
            for k, v in self.checkouts_per_vendor_by_group.items()
        }
        if daemon:
            filtered_checkouts = {
                k: v for k, v in sorted_checkouts.items() if k == daemon
            }
            return template.render(data=filtered_checkouts)
        return template.render(data=sorted_checkouts)

    def html_checkouts_per_user(self, daemon=None):
        report_template = """
  <h1>Checkouts by username</h1>
{%- for daemon,licences in data.items() %}
   <h2>{{ daemon }}</h2>
{%- for licence, checkouts in licences.items() %}
    <h3>{{ licence }}</h3>
     <table border="1">
      <tr>
       <th>Username</th>
       <th>Group</th>
       <th>Checkouts</th>
      </tr>
{%- for user, count in checkouts.items() %}
      <tr>
       <td>{{ user }}</td>
       <td>{{ groups[user] }}</td>
       <td>{{ count }}</td>
      </tr>
{% endfor %}
     </table>
{%- endfor %}
{%- endfor %}
"""
        template = jinja2.Template(report_template)
        sorted_checkouts = defaultdict(dict)
        for daemon_key, licence_dict in self.checkouts_by_person.items():
            for licence, user_checkouts in licence_dict.items():
                sorted_checkouts[daemon_key][licence] = OrderedDict(
                    sorted(
                        user_checkouts.items(), key=lambda t: int(t[1]), reverse=True
                    )
                )

        if daemon:
            filtered_checkouts = {
                k: v for k, v in sorted_checkouts.items() if k == daemon
            }
            return template.render(data=filtered_checkouts, groups=self.users_and_group)
        return template.render(data=sorted_checkouts, groups=self.users_and_group)

    def html_denied_by_daemon(self, daemon=None):
        report_template = """
  <h1>Denied by vendor</h1>
{%- for daemon,checkouts in data.items() %}
   <h2>{{ daemon }}</h2>
     <table border="1">
      <tr>
       <th>Username</th>
       <th>Checkouts</th>
      </tr>
{%- for user, count in checkouts.items() %}
      <tr>
       <td>{{ user }}</td>
       <td>{{ count }}</td>
      </tr>
{% endfor %}
     </table>
{%- endfor %}
"""
        template = jinja2.Template(report_template)
        sorted_checkouts = defaultdict(dict)
        for daemon_key, checkouts in self.all_denied.items():
            sorted_checkouts[daemon_key] = OrderedDict(
                sorted(checkouts.items(), key=lambda t: int(t[1]), reverse=True)
            )

        if daemon:
            filtered_checkouts = {
                k: v for k, v in sorted_checkouts.items() if k == daemon
            }
            return template.render(data=filtered_checkouts)
        return template.render(data=sorted_checkouts)

    def html_denied_by_licence(self, daemon=None):
        report_template = """
  <h1>Denied by licence</h1>
{%- for daemon,licences in data.items() %}
   <h2>{{ daemon }}</h2>
{%- for licence, checkouts in licences.items() %}
    <h3>{{ licence }}</h3>
     <table border="1">
      <tr>
       <th>Username</th>
       <th>Group</th>
       <th>Checkouts</th>
      </tr>
{%- for user, count in checkouts.items() %}
      <tr>
       <td>{{ user }}</td>
       <td>{{ groups[user] }}</td>
       <td>{{ count }}</td>
      </tr>
{% endfor %}
     </table>
{%- endfor %}
{%- endfor %}
"""
        template = jinja2.Template(report_template)
        sorted_checkouts = defaultdict(dict)
        for daemon_key, licence_dict in self.denied_by_person.items():
            for licence, user_checkouts in licence_dict.items():
                sorted_checkouts[daemon_key][licence] = OrderedDict(
                    sorted(
                        user_checkouts.items(), key=lambda t: int(t[1]), reverse=True
                    )
                )

        if daemon:
            filtered_checkouts = {
                k: v for k, v in sorted_checkouts.items() if k == daemon
            }
            return template.render(data=filtered_checkouts, groups=self.users_and_group)
        return template.render(data=sorted_checkouts, groups=self.users_and_group)

    def html_report(self, daemon=None):
        reports = (
            self.html_checkouts_by_group(daemon)
            + self.html_checkouts_per_vendor_by_group(daemon)
            + self.html_checkouts_per_user(daemon)
            + self.html_denied_by_daemon(daemon)
            + self.html_denied_by_licence(daemon)
        )
        template = jinja2.Template(html_boilerplate)
        return template.render(body=reports)


class SchrodStats(FlexLMStats):
    def __init__(self, csvfile):
        FlexLMStats.__init__(self)
        self.csvfile = csvfile
        self.group_dict = {}

    def load_group_dict(self):
        with open(self.csvfile, "r") as f:
            reader = csv.reader(f)
            for row in reader:
                if not row[0] == "User":
                    self.group_dict[row[0]] = row[1]

    def lookup_group_for_user(self, username, cur):
        group = "Unknown"
        if username == "anyone":
            return "All"
        try:
            group = self.group_dict[username]
        except KeyError:
            try:
                sql = (
                    "select name from apps.software_licence_stats where crsid = $$%s$$"
                    % (username)
                )
                cur.execute(sql)
                group = cur.fetchone()[0]
            except TypeError:
                pass
        return group

    def populate_groups(self):
        self.load_group_dict()
        db = psycopg2.connect(
            "host=database.ch.private.cam.ac.uk dbname=chemistry user={0}".format(
                dbuser
            )
        )
        c = db.cursor()
        c.execute("set datestyle to 'European,SQL'")
        for daemon in self.checkouts_by_person:
            for licence in self.checkouts_by_person[daemon]:
                for user in self.checkouts_by_person[daemon][licence]:
                    group = self.lookup_group_for_user(user, c)
                    if group == "Unknown":
                        self.checkouts_with_unknown_group[daemon].setdefault(
                            licence, {}
                        )
                        self.checkouts_with_unknown_group[daemon][licence].setdefault(
                            user, 0
                        )
                        self.checkouts_with_unknown_group[daemon][licence][
                            user
                        ] += self.checkouts_by_person[daemon][licence][user]
                    self.users_and_group[user] = group
                    self.checkouts_by_group[daemon][licence].setdefault(group, 0)
                    self.checkouts_by_group[daemon][licence][
                        group
                    ] += self.checkouts_by_person[daemon][licence][user]
                    self.checkouts_per_vendor_by_group[daemon].setdefault(group, 0)
                    self.checkouts_per_vendor_by_group[daemon][
                        group
                    ] += self.checkouts_by_person[daemon][licence][user]
                    self.all_detailed_checkouts.append(
                        CheckOut(
                            user=user,
                            group=group,
                            daemon=daemon,
                            licence=licence,
                            count=self.checkouts_by_person[daemon][licence][user],
                        )
                    )

        db.close()
